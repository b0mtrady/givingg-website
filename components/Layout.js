import Link from "next/link";
import Head from "next/head";
export default ({ children, title }) => (
  <div className="root">
    <Head>
      <title>Givin.gg</title>
    </Head>
    <header>
      <div className="container">
        <div>
          <Link href="/">
            <a className="logo">Givin.gg</a>
          </Link>
        </div>
        <div></div>
        <div></div>
        <div className="menu-container">
          <div>
            <Link href="/">
              <a>Us</a>
            </Link>
          </div>
          <div>
            <Link href="/">
              <a>Action</a>
            </Link>
          </div>

          <div>
            <Link href="/">
              <a>Education</a>
            </Link>
          </div>
          <div>
            <Link href="/">
              <a>Blog</a>
            </Link>
          </div>
        </div>
      </div>
    </header>
    {children}
    <footer>&copy; {new Date().getFullYear()}</footer>

    <style jsx>{`
      header a {
        color: #53682b;
        text-decoration: none;
      }
      header a:hover {
        font-weight: bold;
        color: #74825c;
      }
      .container{
      display: grid;
      grid-template-columns: repeat(auto-fit,minmax(240px, 1fr));
      grid-template-rows: auto;
      justify-items: center;
      }
      .logo {
        font: 400 60px/1.3 "Berkshire Swash", Helvetica, sans-serif;
        font-size: 7vw;
        text-shadow: 1px 1px 0px #ededed, 4px 4px 0px rgba(0, 0, 0, 0.15);
      }
      .menu-container{
display:grid;
grid-template-columns: auto;
grid-template rows: 1fr 1fr 1fr 1fr;
font-size:2vw;
align-items: center;
text-align:center;
      }
    `}</style>
  </div>
);
